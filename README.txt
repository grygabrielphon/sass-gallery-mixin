Simple Sass Image Gallery Mixin

This is a simple Sass mixin that will allow you to create slick image galleries with slide descriptions: 

http://codepen.io/WereHare/pen/KAkdu

Just import the partial... 

	@import "_image-gallery";

Include your gallery (at the root of your stylesheet - NOT as a property):

	@include gallery(mygallery, 4);

Then make sure you have a div with the same class as your gallery name. The number we passed into the mixin is the number of slides - this MUST match the number of child divs in your markup, otherwise the gallery will behave strangely.

	<div class="mygallery">
		<div class="gallery1">
			<div><h2>Sample slide description</h2></div>
		</div>
		<div class="gallery2">
			<div><h1>Another slide description</h1></div>
		</div>
		<div class="gallery3">
			<div><img src="logo.png"></div>
		</div>
		<div class="gallery4">
		</div>
	</div>

Apply background images to your individual slide divs or put whatever kind of content you want in them.

Grandchild divs are your slide descriptions. Put whatever you want in them, and the mixin will slide them in and out while fading between the child divs. 

 ARGUMENTS ARE: 
  1: name of class/animation 
  2: number of slides (MUST match up with number of children the element has, otherwise it will animate erratically)
  3: duration of each slide (optional)
